package comp2931.cwk1;

import org.junit.Test;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.Calendar;

public class DateTest {
  @Rule
  public ExpectedException error = ExpectedException.none();

  @Test
  public void testString() {
    Date valid = new Date(2000,01,11);
    Date oddInput = new Date(10,2,2);
    Date extreme = new Date(1,1,1);
    assertThat(valid.toString(), is ("2000-01-11"));
    assertThat(oddInput.toString(), is ("0010-02- 2"));
    assertThat(extreme.toString(), is ("0001-01- 1"));
  }

  @Test
  public void outOfBounds() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage("Day out of bounds!");
    Date outOfBounds = new Date(2000,4,31);
  }

  @Test
  public void yearTooLow() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage("Not a valid year!");
    Date yearTooLow = new Date(0,1,1);
  }

  @Test
  public void monthTooHigh() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage("Not a valid month!");
    Date valueTooLow = new Date(1,13,1);
  }

  @Test
  public void leapYear() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage("Last day of leap year February exceeded!");
    Date leapYear = new Date(1776,2,30);
  }

  @Test
  public void extraDayOnNonLeapYear() {
    error.expect(IllegalArgumentException.class);
    error.expectMessage("The year is not a leap year. No value should be higher than 28!");
    Date extraDay = new Date(1775,2,29);
  }

  @Test
  public void testEquals() {
    Date date = new Date(1000,10,10);
    Date date2 = new Date(1000,10,10);
    Date date3 = new Date(100,10,10);
    Date date4 = new Date(1000,10,1);
    assertThat(date.equals(date2), is (true));
    assertThat(date.equals(date3), is (false));
    assertThat(date3.equals(date3), is (true));
    assertThat(date2.equals(date4), is (false));
    assertThat(date.equals("String"), is (false));
  }

  @Test
  public void testDayOfYear() {
    Date randomDay = new Date(1000,5,26);
    Date firstDay = new Date(2000,1,1);
    Date lastDay = new Date(3000,12,31);
    assertThat(randomDay.getDayOfYear(), is (146));
    assertThat(firstDay.getDayOfYear(), is (1));
    assertThat(lastDay.getDayOfYear(), is (365));
  }

  @Test
  public void testTodayDate() {
    Date today = new Date();
    Calendar c = Calendar.getInstance();
    Date compare = new Date(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));
    assertThat(today.equals(compare), is (true));
  }
}

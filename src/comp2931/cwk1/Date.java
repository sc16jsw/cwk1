// Class for COMP2931 Coursework 1

package comp2931.cwk1;

import java.util.Calendar;

/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   * @throws IllegalArgumentException When the date is outside of boundaries
   */
  public Date(int y, int m, int d) throws IllegalArgumentException {
    if (y < 1) {
      throw new IllegalArgumentException("Not a valid year!");
    }
    else if (m < 1 || m > 12) {
      throw new IllegalArgumentException("Not a valid month!");
    }
    else if (d < 1) {
      throw new IllegalArgumentException("Not a valid day!");
    }
    else if ((y % 4) == 0 && m == 2 && d > 29) {
      throw new IllegalArgumentException("Last day of leap year February exceeded!");
    }
    else if ((y % 4) != 0 && m == 2 && d > 28) {
      throw new IllegalArgumentException("The year is not a leap year. No value should be higher than 28!");
    }
    else if ((m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) && d > 31) {
      throw new IllegalArgumentException("Day out of bounds!");
    }
    else if ((m == 2 || m == 4 || m == 6 || m == 9 || m == 11) && d > 30) {
      throw new IllegalArgumentException("Day out of bounds!");
    }
    else {
      year = y;
      month = m;
      day = d;
    }
  }

  /**
   * Creates a date by calling the instance of today's date
   */
  public Date() {
    Calendar cal = Calendar.getInstance();
    year = cal.get(Calendar.YEAR);
    month = cal.get(Calendar.MONTH) + 1;
    day = cal.get(Calendar.DAY_OF_MONTH);
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Returns the day of the year of this date.
   *
   * @return Day of the year
   */
  public int getDayOfYear() {
    int dayNumber = 0;
    if (month > 1) { dayNumber = dayNumber + 31; }
    if (month > 2) { dayNumber = dayNumber + 28; }
    if (month > 3) { dayNumber = dayNumber + 31; }
    if (month > 4) { dayNumber = dayNumber + 30; }
    if (month > 5) { dayNumber = dayNumber + 31; }
    if (month > 6) { dayNumber = dayNumber + 30; }
    if (month > 7) { dayNumber = dayNumber + 31; }
    if (month > 8) { dayNumber = dayNumber + 31; }
    if (month > 9) { dayNumber = dayNumber + 30; }
    if (month > 10) { dayNumber = dayNumber + 31; }
    if (month > 11) { dayNumber = dayNumber + 30; }
    dayNumber = dayNumber + day;
    return dayNumber;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", year, month, day);
  }

  /**
   * Returns a boolean of whether this date is equal to the date in the parameter
   *
   * @param o Object
   * @return True if they are equal, false otherwise
   */
  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Date)) {
      return false;
    }

    Date d = (Date) o;

    if (d.year != year) {
      return false;
    }
    if (d.month != month) {
      return false;
    }
    if (d.day != day) {
      return false;
    }
    return true;
  }
}
